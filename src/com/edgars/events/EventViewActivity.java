package com.edgars.events;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class EventViewActivity extends Activity{
	
	public static final String TAG = "EventViewActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Fragment fragment = new EventViewFragment();
    	FragmentTransaction ft = getFragmentManager().beginTransaction();
    	ft.add(R.id.container, fragment).commit();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Log.i(TAG, "onBackPressed");
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(intent);
	}
	
}
