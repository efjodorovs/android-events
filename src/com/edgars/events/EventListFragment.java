package com.edgars.events;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class EventListFragment extends ListFragment {
	
	public static final String TAG = "EventListFragment";
	public static final int REQUEST_CODE = 0;
	final int FIVE_MINUTES = 300000;
    final int ONE_MINUTE = 60000;
    final int THIRTY_SECONDS = 30000;
    final int THREE_HOURS = 60000 * 60 * 3; // millis, minutes, hours

    ListView listView;
    View view;
    Context context;
    DatabaseHelper dbHelper;
    ImageView eventImageView;
    TextView eventNameTextView;
    TextView eventStartDateTextView;
    ArrayList<Event> list;
    EventAdapter eventAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    int positionItem;
    int eventId;
    AlarmManager alarmManager;
    JsonTask task;
    Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.i(TAG, "onCreate");
        populateList();      
    }
    
    
    
    @Override
    public void onStart() {
    	super.onStart();
    	Log.i(TAG, "setAlarm");
    	setAlarm();  	
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
    	View view = inflater.inflate(R.layout.fragment_event_list, container, false);
    	
    	listView = (ListView)view.findViewById(android.R.id.list);
    	
    	listView.setClickable(true);
    	listView.setItemsCanFocus(true);
    	
    	swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe); 
    	
    	swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
    		
    		@Override
    		public void onRefresh() {
    			Log.i(TAG, "onRefresh");
    			JsonTask.accessWebService();
    			populateList();
    			handler.post(refresh);
    		}
    	});
    	    	
    	return view;
    }
    
    private final Runnable refresh = new Runnable() {

		@Override
		public void run() {
			handler.postDelayed(refresh, 3000);
			swipeRefreshLayout.setRefreshing(false);
		}
    	
    };
    
    public void populateList() {
    	Log.i(TAG, "populateList");
    	dbHelper = new DatabaseHelper(getActivity());
        try {
			dbHelper.open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        	
        list = dbHelper.getEvents();

        Collections.sort(list, new Comparator<Event>() {

			@Override
			public int compare(Event lhs, Event rhs) {
				if (lhs.getStartDate() == null || rhs.getStartDate() == null) return 0;
				return lhs.getStartDate().compareTo(rhs.getStartDate());
			}
        	
        });
        
        eventAdapter = new EventAdapter(list);
        setListAdapter(eventAdapter);

        dbHelper.close();
    }
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	Log.i(TAG, "onListItemClick");
    	
    	Event event = (Event) l.getItemAtPosition(position);
    	eventId = event.getId();
    	Intent intent = new Intent(getActivity(), EventViewActivity.class);
		intent.putExtra("id", eventId);
		startActivity(intent);
    }
    
    public void setAlarm() {
    	Log.i(TAG, "setAlarm");
    	Context context = getActivity();
    	Intent intent = new Intent(context, AlarmService.class);
        
        PendingIntent pendingIntent = PendingIntent.getService(context, REQUEST_CODE, intent, REQUEST_CODE);
        int alarmType = AlarmManager.ELAPSED_REALTIME;
            	
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(alarmType, SystemClock.elapsedRealtime() + THREE_HOURS,
        		THREE_HOURS, pendingIntent);
        //alarmManager.cancel(pendingIntent);
        Log.i(TAG, "Alarm set!");
	}
        
	public void initiateRefresh() {
		Log.i(TAG, "initiateRefresh");
		//new JsonTask().execute();
		Fragment fragment = new EventListFragment();
    	FragmentTransaction ft = getFragmentManager().beginTransaction();
    	ft.add(R.id.container, fragment).commit();
	}
	
	public void onRefreshComplete() {
		Log.i(TAG, "onRefreshComplete");
		
		swipeRefreshLayout.setRefreshing(false);
	}
	
    public class EventAdapter extends ArrayAdapter<Event> {
    	
    	public EventAdapter(ArrayList<Event> e) {
    		super(getActivity(), android.R.layout.simple_list_item_1, e);
    	}
    	
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
    		Log.i(TAG, "getView");
    		if(convertView == null) {
    			convertView = getActivity().getLayoutInflater().inflate(R.layout.event_list_item, null);
    		}
    		Event event = getItem(position);
    		eventNameTextView = (TextView)convertView.findViewById(R.id.event_list_name);
    		eventNameTextView.setText(event.getName());
    		eventNameTextView.setFocusable(false);
    		eventStartDateTextView = (TextView)convertView.findViewById(R.id.event_list_date);
    		eventStartDateTextView.setText(event.getStartDate()); 
    		eventStartDateTextView.setFocusable(false);
    		
    		return convertView;
    	}	
    }
        
    @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.main, menu);
	}
        
}
