package com.edgars.events;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class JsonTask extends AsyncTask<String, Void, String> {

    private static final String TAG = "JsonTask";
    private static final String site = "http://events.graphicstories.net";
    private static final String url = site + "/json.php";
    
    Context context;
    String jsonResult;
    DatabaseHelper dbHelper;
    ArrayList<Event> eventList;
    Event event;
    
	public static void accessWebService() {
    	Log.i(TAG, "accessWebService");
        JsonTask task = new JsonTask();
        task.execute(new String[] { url });
    }

    public void loadJSON() throws Exception {
        eventList = new ArrayList<Event>();
        Log.i(TAG, "loadJSON");      
        try {
            JSONObject jsonResponse = new JSONObject(jsonResult);
            JSONArray jsonMainNode = jsonResponse.optJSONArray("event");
                for (int i = 0; i < jsonMainNode.length(); i++) {
                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                String event_name = jsonChildNode.optString("event_name");
                String event_image = jsonChildNode.optString("event_image");
                
                String imgurl = site + event_image;
                System.out.println("loadJSON imgurl: " + imgurl);
                
                String event_start_date = jsonChildNode.optString("start_date");
                String event_end_date = jsonChildNode.optString("end_date");
                String event_price = jsonChildNode.optString("price");
                String event_other_details = jsonChildNode.optString("event_other_details");
                String event_category = jsonChildNode.optString("categories");
                event = new Event();
                event.name = event_name;
                event.image = getPicture(imgurl);
                event.startDate = event_start_date;
                event.endDate = event_end_date;
                event.price = event_price;
                event.details = event_other_details;
                event.category = event_category;
                eventList.add(event);
            }

        } catch (JSONException e) {
            Toast.makeText(context, R.string.error + e.toString(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void loadDatabase() {
        Log.i(TAG, "loadDatabase");
        for (int i = 0; i < eventList.size(); i++) {
        	dbHelper = new DatabaseHelper(context);
        	dbHelper.loadEvent(new Event(
        			eventList.get(i).name,
        			eventList.get(i).image,
        			eventList.get(i).startDate,
        			eventList.get(i).endDate,
        			eventList.get(i).price,
        			eventList.get(i).details,
        			eventList.get(i).follow,
        			eventList.get(i).category
        	));
        }
    }

    private StringBuilder inputStreamToString(InputStream is) {
    	Log.i(TAG, "inputStreamToString");
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        }

        catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context,
                    R.string.error + e.toString(), Toast.LENGTH_LONG).show();
        }
        return answer;
    }
    
    public byte[] getPicture(String url){
    	Log.i(TAG, "getPicture");
        try {
            URL imageUrl = new URL(url);
            URLConnection ucon = imageUrl.openConnection();

            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

            ByteArrayBuffer baf = new ByteArrayBuffer(500);
            int current = 0;
            while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
            }
            return baf.toByteArray();
        } catch (Exception e) {
            Log.d("ImageManager", "Error: " + e.toString());
        }
        return null;
    }

    @Override
    protected String doInBackground(String... params) {
        Log.i(TAG, "doInBackground");
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(params[0]);
        try {
            HttpResponse response = httpclient.execute(httppost);
            jsonResult = inputStreamToString(
                    response.getEntity().getContent()).toString();
        }
        catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
			loadJSON();
		} catch (Exception e) {
			e.printStackTrace();
		}
        loadDatabase();
        
        return null;
    }
    
    @Override
    protected void onPreExecute() {
    	super.onPreExecute();
    	Log.i(TAG, "onPreExecute");
    }

    @Override
    protected void onPostExecute(String result) {
	   	super.onPostExecute(result);
        Log.i(TAG, "onPostExecute");
    }
   
}
