package com.edgars.events;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class AlarmService extends Service {

	private static final int NOTIFICATION_ID = 1;
	public static final String TAG = "AlarmService";
	
	NotificationManager notificationManager;
	PendingIntent pendingIntent;
	ArrayList<Event> list;
	DatabaseHelper dbHelper;
	//Context context;
	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG, "onCreate");
	}
	   
    @Override
    public IBinder onBind(Intent arg0) {
    	Log.i(TAG, "onBind");
        return null;
    }
    	
	@Override
	public void onStart(Intent intent, int startId) {
		Log.i(TAG, "onStart");

		dbHelper = new DatabaseHelper(getApplicationContext());
        try {
			dbHelper.open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        	
        list = dbHelper.getEvents();
        System.out.println("List: " + list);

        for (int i = 0; i < list.size(); i++) {
        	        	
        	if (list.get(i).getFollow() == 1) {
        		
        		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            	System.out.println("Not formated: " + list.get(i).getStartDate());
            	String startDate = list.get(i).getStartDate();
            	try {
    				startDate = sdf.format(sdf.parse(startDate)).toString();
    				System.out.println("Formated date: " + startDate);
    			} catch (java.text.ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
            	
            	Calendar c = Calendar.getInstance();
            	String currentDate = c.getTime().toString();
            	currentDate = sdf.format(new Date());
            	System.out.println("Current date: " + currentDate);
        		
        		if (startDate.compareTo(currentDate) == 0) {
            		System.out.println("True, event is today!");
            		
            		SimpleDateFormat stf = new SimpleDateFormat("HH:mm");

                	Calendar cc = Calendar.getInstance();
                	String currentTime = cc.getTime().toString();
                	System.out.println("Current time without parse: " + currentTime);
                	currentTime = stf.format(cc.getTime());
        			System.out.println("Current time: " + currentTime);
        			
        			System.out.println("Event startTime: " + list.get(i).getStartDate());
        			String startTime = list.get(i).getStartDate();
        						
        			String[] srcDateArray = startTime.split(" ");
        			String time = srcDateArray[1];
        			System.out.println("Time: "+ time);
        			
        			
        			String[] timeArray = time.split(":");
        			String timeHour = timeArray[0];
        			System.out.println("Event Time: "+ timeHour);

        			
        			String[] currentTimeArray = currentTime.split(":");
        			String currentHour = currentTimeArray[0];
        			System.out.println("Current Time: "+ currentHour);
        			
                	int current = Integer.parseInt(currentHour);
                	int event = Integer.parseInt(timeHour);
                	int result = event - current;
                	System.out.println("Result: " + result);
                	if (result >= 0) {
                		int temp = result;
                		System.out.println("Result: " + temp);
                	} else if (result < 0) {
                		int temp;
                		temp = 24 + result;
                		System.out.println("Result: " + temp);
                	}
            		
            		if (result >= 0 && result <= 3) {
            			Context context = getApplicationContext();
                        Intent mIntent = new Intent(context, EventViewActivity.class);
                        mIntent.putExtra("id", list.get(i).getId());
                        pendingIntent = PendingIntent.getActivity(context, 0, mIntent, PendingIntent.FLAG_CANCEL_CURRENT);   
                                       
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                        builder.setContentTitle(list.get(i).name);
                        builder.setContentText(list.get(i).startDate);
                        builder.setSmallIcon(R.drawable.ic_launcher);
                        builder.setAutoCancel(true);
                        builder.setContentIntent(pendingIntent);
                        
                        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.notify(NOTIFICATION_ID, builder.build());
            		} else {
            			System.out.println("No events in these hours..");
            		}
            	} else {
            		System.out.println("False, event is not today..");
            	}
        	}
        }
        dbHelper.close();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "onDestroy");
	}

}
