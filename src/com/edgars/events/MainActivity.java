package com.edgars.events;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends Activity {
	
	private static final String TAG = "MainActivity";
	public static final String ERROR_MESSAGE = "";
	
	//DatabaseHelper dbHelper;
	//Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); 
        isNetworkConnected();
        
        if (savedInstanceState == null) {
        	EventListFragment fragment = new EventListFragment();
        	android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
        	ft.add(R.id.container, fragment).commit();
        }
    }
    
    private boolean isNetworkConnected() {
    	Log.i(TAG, "isNetworkConnected");
    	ConnectivityManager cm = (ConnectivityManager)   getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo networkInfo = cm.getActiveNetworkInfo();
    	if (networkInfo == null) {
    		Log.i(TAG, "false");
    		Toast.makeText(getApplicationContext(), R.string.noInternetConnection, Toast.LENGTH_LONG).show();
    		EventListFragment fragment = new EventListFragment();
        	android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
        	ft.add(R.id.container, fragment).commit();
        	return false;
    	} else {
    		Log.i(TAG, "true");
    		JsonTask.accessWebService();
    		return true;
    	}
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
	public void onBackPressed() {
		super.onBackPressed();
		Log.i(TAG, "onBackPressed");
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		startActivity(intent);
	}
}

