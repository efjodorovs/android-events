package com.edgars.events;

import java.sql.SQLException;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

public class EventViewFragment extends Fragment {

	private static final String TAG = "EventViewFragment";
	
	ImageView eventImageView;
	TextView eventNameTextView;
	TextView eventStartDateTextView;
	TextView eventEndDateTextView;
	TextView eventPriceTextView;
	TextView eventDetailsTextView;
	CheckBox eventFollowBox;
	Event event;
	DatabaseHelper dbHelper;
	String name;
	int checked = 0;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = getActivity().getIntent();
		int id = (Integer) intent.getSerializableExtra("id");
		Log.i(TAG, "onCreate get id: " + id);
		
		dbHelper = new DatabaseHelper(getActivity());
        try {
			dbHelper.open();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		event = dbHelper.getEvent(id);
		
		dbHelper.close();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_view_event, container, false);
		
		eventFollowBox = (CheckBox)view.findViewById(R.id.follow);
		if(event.getFollow() == 0) {
			eventFollowBox.setChecked(false);
		} else {
			eventFollowBox.setChecked(true);
		}
		
		if(event.getImage() == null) {
			eventImageView = (ImageView)view.findViewById(R.id.event_view_image);
			eventImageView.setBackgroundResource(R.drawable.defaultnophoto);
		} else {
			eventImageView = (ImageView)view.findViewById(R.id.event_view_image);
			eventImageView.setImageBitmap(BitmapFactory.decodeByteArray(event.getImage(), 0, event.getImage().length));
		}
		
		eventNameTextView = (TextView)view.findViewById(R.id.event_view_name);
		eventNameTextView.setText(event.getName());
		
		eventStartDateTextView = (TextView)view.findViewById(R.id.event_view_start_date);
		eventStartDateTextView.setText(event.getStartDate());
		
		eventEndDateTextView = (TextView)view.findViewById(R.id.event_view_end_date);
		eventEndDateTextView.setText(event.getEndDate());
		
		eventPriceTextView = (TextView)view.findViewById(R.id.event_view_price);
		eventPriceTextView.setText(event.getPrice());
		
		eventDetailsTextView = (TextView)view.findViewById(R.id.event_view_details);
		eventDetailsTextView.setText(event.getDetails());
		
		eventFollowBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				Log.i(TAG, "onCheckedChanged");
				
				dbHelper = new DatabaseHelper(getActivity());
		        try {
					dbHelper.open();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        if(isChecked) {
		        	checked = 1;
		        } else {
		        	checked = 0;
		        }
				dbHelper.updateFollow(event, checked);
				dbHelper.close();
				}
			});
		
		return view;
	}
	
	
	
}
