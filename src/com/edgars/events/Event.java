package com.edgars.events;


public class Event {

    int id;
    String name;
    byte[] image;
    String startDate;
    String endDate;
    String price;
    String details;
    String category;
    int follow;

	public Event(){};

    public Event(String eventName, byte[] eventImage, String eventStartDate,
                 String eventEndDate, String eventPrice, String eventDetails, int eventFollow, String eventCategory) {
        this.name = eventName;
        this.startDate = eventStartDate;
        this.endDate = eventEndDate;
        this.image = eventImage;
        this.price = eventPrice;
        this.details = eventDetails;
        this.category = eventCategory;
        this.follow = eventFollow;
    }

    public int getId() {
        return id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    public int getFollow() {
		return follow;
	}

	public void setFollow(int follow) {
		this.follow = follow;
	}

}
