package com.edgars.events;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver{
	
	public static final String TAG = "AlarmReceiver";
	
	NotificationManager notificationManager;
	PendingIntent pendingIntent;

	@Override
	public void onReceive(Context context, Intent i) {
		Log.i(TAG, "onReceive");
		
		Intent service = new Intent(context, AlarmService.class);
        context.startService(service);
	}
	
}
