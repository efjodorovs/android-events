package com.edgars.events;

import java.sql.SQLException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TAG ="DatabaseHelper";

    public static final String EVENT_TABLE_NAME = "event";
    public static final String COLUMN_EVENT_ID = "id";
    public static final String COLUMN_EVENT_NAME = "event_name";
    public static final String COLUMN_EVENT_IMAGE = "event_image";
    public static final String COLUMN_EVENT_START_DATE = "event_start_date";
    public static final String COLUMN_EVENT_END_DATE = "event_end_date";
    public static final String COLUMN_EVENT_PRICE = "event_price";
    public static final String COLUMN_EVENT_DETAILS = "event_other_details";
    public static final String COLUMN_EVENT_CATEGORY = "event_category";
    public static final String COLUMN_EVENT_FOLLOW = "event_follow";
    public static final String DATABASE_NAME = "Events.db";
    public static final int DATABASE_VERSION = 1;

    private static final String CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " +
                    EVENT_TABLE_NAME + " ( " +
                    COLUMN_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
		            COLUMN_EVENT_NAME + " TEXT UNIQUE, " +
		            COLUMN_EVENT_IMAGE + " TEXT, " +
		            COLUMN_EVENT_START_DATE + " TEXT, " +
		            COLUMN_EVENT_END_DATE + " TEXT, " +
		            COLUMN_EVENT_PRICE + " TEXT, " +
		            COLUMN_EVENT_DETAILS + " TEXT, " +
		            COLUMN_EVENT_FOLLOW + " INTEGER DEFAULT 0, " +
		            COLUMN_EVENT_CATEGORY + " TEXT " + ")";
    private static final String DELETE_ENTRIES =
            "DROP TABLE IF EXISTS" + EVENT_TABLE_NAME;

    Context context;
    static DatabaseHelper dbHelper;
    SQLiteDatabase db;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
    
    public static synchronized DatabaseHelper getInstance(Context context) {
    	if (dbHelper == null) {
    		dbHelper = new DatabaseHelper(context.getApplicationContext());
    	}
    	return dbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ENTRIES);
        Log.i(TAG, "onCreate");
        
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Updating database from version: " + oldVersion + " to version: " + newVersion);
        db.execSQL(DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    public void loadEvent(Event event) {
        Log.i(TAG, "loadEvent");
        ContentValues cv = new ContentValues();
        db = dbHelper.getWritableDatabase();

        cv.put(COLUMN_EVENT_NAME, event.getName());
        cv.put(COLUMN_EVENT_IMAGE, event.getImage());
        cv.put(COLUMN_EVENT_START_DATE, event.getStartDate());
        cv.put(COLUMN_EVENT_END_DATE, event.getEndDate());
        cv.put(COLUMN_EVENT_PRICE, event.getPrice());
        cv.put(COLUMN_EVENT_DETAILS, event.getDetails());
        cv.put(COLUMN_EVENT_FOLLOW, event.getFollow());
        cv.put(COLUMN_EVENT_CATEGORY, event.getCategory());
        db.insert(EVENT_TABLE_NAME, null, cv);

    }
    
    public ArrayList<Event> getEvents() {
    	ArrayList<Event> list = new ArrayList<Event>();
    	String query = "SELECT * FROM " + EVENT_TABLE_NAME;
    	Log.i(TAG, "getEvents");
    	Log.e(TAG, query);
    	
    	Cursor cursor = db.rawQuery(query, null);
    	if (cursor.moveToFirst()) {
    		do {
    			Event event = new Event();
    			event.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_EVENT_ID)));
    			event.setName(cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_NAME)));
    			event.setStartDate(cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_START_DATE)));
    			event.setEndDate(cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_END_DATE)));
    			event.setPrice(cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_PRICE)));
    			event.setDetails(cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_DETAILS)));
    			event.setFollow(cursor.getInt(cursor.getColumnIndex(COLUMN_EVENT_FOLLOW)));
    			event.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_CATEGORY)));
    			list.add(event);
    		} while (cursor.moveToNext());
    	}
    	return list;
    }
    
    public Event getEvent(int id) {
    	Event event = new Event();
    	Log.i(TAG, "getEvent");
    	Cursor cursor = db.rawQuery("SELECT * FROM " + 
	    	EVENT_TABLE_NAME + 
	    	" WHERE " + 
	    	COLUMN_EVENT_ID + 
	    	" = " + 
	    	id, 
	    	null);
    	
    	if (cursor.moveToFirst()) {
    		do {
    			event.id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_ID)));
        		event.name = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_NAME));
        		event.image = cursor.getBlob(cursor.getColumnIndex(COLUMN_EVENT_IMAGE));
    			event.startDate = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_START_DATE));
    			event.endDate = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_END_DATE));
    			event.price = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_PRICE));
    			event.details = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_DETAILS));
    			event.category = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_CATEGORY));
    			event.follow = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_FOLLOW)));
    		} while (cursor.moveToNext());
    	}
    	cursor.close();
    	return event;
    }
    
    public int updateFollow(Event event, int checked) {
    	Log.i(TAG, "updateFollow");
    	
    	ContentValues cv = new ContentValues();
    	cv.put(COLUMN_EVENT_FOLLOW, checked);
    	
    	return db.update(EVENT_TABLE_NAME, 
    			cv, 
    			COLUMN_EVENT_ID + " = ?", 
    			new String[] {String.valueOf(event.getId())});
    }

    public DatabaseHelper open() throws SQLException {
        Log.i(TAG, "Open");
        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        Log.i(TAG, "Close");
        if (db != null && db.isOpen()) db.close();
    }
}
